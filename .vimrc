" Enable syntax highlighting
syntax on

set smarttab
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set smartindent
set cindent
set cmdheight=2
set wildmenu
set langmenu=en
set magic
set showmode
set incsearch
set ruler
set hid
set whichwrap+=<,>,h,l
set mouse=a
set numberwidth=6
set encoding=utf-8
set cursorline
set backspace=2
set clipboard=unnamedplus

" Map F6 to spellchecking
map <F6> :setlocal spell! spelllang=en_us<CR>

"Auto toggle between absolute and relativ numbering, when switching between insert and normal mode
set number relativenumber

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

colorscheme slate

" Disable Terminal bell
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=
