killall polybar
polybar -m | while read m; do
    if [[ $m == *"primary"* ]]; then
        export TRAY=right
    else
        export TRAY=none
    fi
    export MONITOR=$(echo $m | cut -d ":" -f1)
    polybar -r top &
done
