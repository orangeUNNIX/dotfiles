" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

"set shellslash

call plug#begin()

"Auto completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Debugging
"Plug 'puremourning/vimspector'
"let g:vimspector_enable_mappings='HUMAN'

" Nerdtree file explorer
Plug 'preservim/nerdtree'

" Automatically close brackets
Plug 'jiangmiao/auto-pairs'

" fzf - Fuzzy finder
Plug 'junegunn/fzf'

" Multi cursor (like sublime)
Plug 'terryma/vim-multiple-cursors'

" 'Vim sugar for the UNIX shell commands that need it the most.'
Plug 'tpope/vim-eunuch'

" Surround selection with e.g. ()
Plug 'tpope/vim-surround'

" HTML and CSS
Plug 'mattn/emmet-vim'

" CSS color preview
Plug 'ap/vim-css-color'

" Color theme
Plug 'tomasiser/vim-code-dark'

" Status/tabline
Plug 'vim-airline/vim-airline'

" ... and themes
Plug 'vim-airline/vim-airline-themes'

call plug#end()


" Enable syntax highlighting
syntax on

set smarttab
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set smartindent
set cindent
set cmdheight=2
set wildmenu
set langmenu=en
set magic
set showmode
set incsearch
set ruler
set hid
set whichwrap+=<,>,h,l
set mouse=a
set numberwidth=6
set encoding=utf-8
set cursorline
set cursorcolumn
set backspace=2
set clipboard=unnamed
set guifont=MesloLGS\ NF:hg

" Map F6 to spellchecking
map <F6> :setlocal spell! spelllang=en_us<CR>

" Nerdtree map
map <C-t> :NERDTreeToggle<CR>

" FZF map
map <C-k> :FZF<CR>


" Move lines up and down
noremap <A-Down> :m+1<Enter>==
noremap <A-Up> :m-2<Enter>==


" Strg + S as save (because of habit)
nnoremap <C-s> :w<Enter>


" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction


"Auto toggle between absolute and relativ numbering, when switching between insert and normal mode
set number relativenumber

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set norelativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set relativenumber
augroup END

colorscheme codedark

" Disable Terminal bell
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=
