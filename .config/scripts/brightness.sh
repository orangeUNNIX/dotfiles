#!/bin/bash

bf="/home/moritz/.config/brightness.txt"
min=3
max=100

if [ ! -f "$bf" ]; then
    echo $(xbacklight -get) | awk '{print int($1+0.5)}' > "$bf"
fi

br=$(<"$bf")


if [ $1 = dec ]; then
    if [ $br -lt 11 ]; then
        br=$((br-1))
    else 
        br=$((br-10))
    fi
elif [ $1 = inc ]; then
    if [ $br -lt 10 ]; then
        br=$((br+1))
    else 
        br=$((br+10))
    fi
fi


if [ $br -gt $max ]; then
    br=$max
fi

if [ $br -lt $min ]; then
    br=$min
fi

echo $br > "$bf"
xbacklight -set $br -time 80 -steps 100

