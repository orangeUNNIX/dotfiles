# More colors
use_color=true

# Shorter programs
alias g=git
alias v=vim

# Aliases
git() { if [[ $@ == "tree" ]]; then command git log --all --graph --oneline; else command git $@; fi; }
alias ll="ls -la"

# After painful experience, the following line was added
alias rm="rm -i"

# Shorter  directories
alias cdg='cd /d/Benutzer/Moritz/Git/'
